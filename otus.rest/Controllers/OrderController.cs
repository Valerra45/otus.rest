﻿using Microsoft.AspNetCore.Mvc;
using otus.rest.Data;
using otus.rest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus.rest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly AppDbContext _appDbContext;

        public OrderController(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        // GET: api/<ProductsController>
        [HttpGet]
        public IEnumerable<Order> Get()
        {
            return this._appDbContext.Orders.ToArray();
        }

        // GET api/<ProductsController>/5
        [HttpGet("{id}")]
        public Order Get(int id)
        {
            return this._appDbContext.Orders.Where(x => x.Id == id)
                .FirstOrDefault();
        }

        // POST api/<ProductsController>
        [HttpPost]
        public async Task<ActionResult<Order>> Post([FromBody] Order order)
        {
            this._appDbContext.Orders.Add(order);
            await this._appDbContext.SaveChangesAsync();

            return CreatedAtAction(nameof(Post), new { id = order.Id }, order);
        }

        // PUT api/<ProductsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Order order)
        {
            var o = this._appDbContext.Orders.FirstOrDefault(x => x.Id == id);

            o.Number = order.Number;
            o.Customer = order.Customer;

            this._appDbContext.SaveChangesAsync();
        }

        // DELETE api/<ProductsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var o = this._appDbContext.Orders.FirstOrDefault(x => x.Id == id);
            this._appDbContext.Remove(o);

            this._appDbContext.SaveChangesAsync();
        }
    }
}
