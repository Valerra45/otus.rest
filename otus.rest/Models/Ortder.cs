﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus.rest.Models
{
    public class Order
    {
        public int Id { get; set; }

        public int Number { get; set; }
        public string Customer { get; set; }
    }
}
